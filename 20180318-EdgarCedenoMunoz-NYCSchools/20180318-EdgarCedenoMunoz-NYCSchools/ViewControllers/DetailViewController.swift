//
//  DetailViewController.swift
//  20180318-EdgarCedenoMunoz-NYCSchools
//
//  Created by Edgar Cedeño-Muñoz on 3/19/18.
//  Copyright © 2018 Edgar Cedeño-Muñoz. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController
{
  @IBOutlet weak var mathSATScoreLabel: UILabel!
  @IBOutlet weak var readingSATScoreLabel: UILabel!
  @IBOutlet weak var writingSATScoreLabel: UILabel!

  func configureView()
  {
    if let school = school
    {
      title = school.schoolName
      //mathSATScoreLabel.text = "\(school.mathSATScore)"
      //readingSATScoreLabel.text = "\(school.readingSATScore)"
      //writingSATScoreLabel.text = "\(school.writingSATScore)"
    }
  }

  override func viewDidLoad()
  {
    super.viewDidLoad()
    configureView()
  }

  override func didReceiveMemoryWarning()
  {
    super.didReceiveMemoryWarning()
  }

  var school: School?
  {
    didSet
    {
      configureView()
    }
  }
}
