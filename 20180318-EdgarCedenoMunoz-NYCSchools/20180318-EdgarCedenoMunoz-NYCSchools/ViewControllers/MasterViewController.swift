//
//  MasterViewController.swift
//  20180318-EdgarCedenoMunoz-NYCSchools
//
//  Created by Edgar Cedeño-Muñoz on 3/19/18.
//  Copyright © 2018 Edgar Cedeño-Muñoz. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController
{

  var detailViewController: DetailViewController? = nil
  var schoolList = [School]()
  
  override func viewDidLoad()
  {
    super.viewDidLoad()

    if let split = splitViewController
    {
      let controllers = split.viewControllers
      detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
    }
  }

  override func viewWillAppear(_ animated: Bool)
  {
    clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
    super.viewWillAppear(animated)
    //let dataCom = NYCDataCom.dataCom
    //dataCom.collectListOfSchools()
    //let school = School()
    //dataCom.getSATScoresUsing(school)
  }

  override func didReceiveMemoryWarning()
  {
    super.didReceiveMemoryWarning()
  }

  // MARK: - Segues

  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if segue.identifier == "showDetail"
    {
      if let indexPath = tableView.indexPathForSelectedRow
      {
        let school = schoolList[indexPath.row]
        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
        controller.school = school
        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        controller.navigationItem.leftItemsSupplementBackButton = true
      }
    }
  }

  // MARK: - Table View

  override func numberOfSections(in tableView: UITableView) -> Int
  {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    return schoolList.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

    let school = schoolList[indexPath.row]
    cell.textLabel?.text = school.schoolName
    return cell
  }

  override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
  {
    return false
  }
}
